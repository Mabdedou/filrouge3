class Cart {

    constructor(){

        this.articles = [] 
        this.total = 0
    }

    addArticle(article) {
        this.articles.push(article)
        this.total += article.price
    }

    removeArticle(index){
        this.total -= this.articles[index].price
        this.articles.splice(index,1) 
    }

}

 


